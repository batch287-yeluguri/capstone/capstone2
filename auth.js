
const jwt = require("jsonwebtoken");


const secret = "RegisterAPI";


module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data) => {

			if(err){
				return res.send({auth: "failed"});
			} else {

				// It Allows the application to proceed with the nest middleware function/callback functin in the route
				next();
			}
		})
	} else {

		 return res.send({ auth: "failed"});
	};
};

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token= token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return null;
			} else {

				// The "decode" method is used to obtain the information from the JWT
				return jwt.decode(token, {complete:  true}).payload;
			}
		})
	};
};