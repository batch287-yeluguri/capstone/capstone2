const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth")

module.exports.registerUser = (reqBody) => {

	console.log(reqBody)

	let newUser = new User({
		name: reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, abc) => {

		if(abc){
			return false
		} else {
			return true
		};
	});
};

module.exports.authenticateUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			} else {

				//Allows the application to proceed with the next middleware function/ callback function in the route
				return false
			};
		};
	});
};


module.exports.getProfile = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		return result;
	});
};


module.exports.checkout = async (data) => {
	console.log(data);
	let variable = 0

	if(!data.isAdmin){

	for(let i=0; i<data.products.length; i++){
		let productPrice= await Product.findById(data.products[i].productId).then(result => {
			return result.price;
		});
		variable = variable+ (data.products[i].quantity)*productPrice;
	}
		
		

		let newOrder = new Order({
			userId: data.userId,
			products: data.products,
			totalAmount: variable
		})
		console.log(data.products[0].productId)
		console.log(newOrder)

		// Saves the created object to our database
		return newOrder.save().then((order, error) => {

			// Course creation failed
			if(error){

				return false;

			// Course creation successful
			} else {

				return true;
			}
		})

	}

	
	let message = Promise.resolve("User must be an NON-Admin to access this!");
	return message.then((value) => {
		return value
	});
	
};



