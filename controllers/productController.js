const Product = require("../models/Product");
const auth = require("../auth")

module.exports.addProduct = (data) => {

	console.log(data);

	if(data.isAdmin){

	
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Course creation failed
			if(error){

				return false;

			// Course creation successful
			} else {

				return true;
			}
		})


	}

	
	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
	
};


module.exports.activeProducts = () => {

	return Product.find({ isActive: true }).then(result => {
		return result;
	});
};

module.exports.getProduct = (reqParams) => {


	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (reqParams, reqBody) => {


		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};


		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			};

		});


		let message = Promise.resolve("User must be an Admin to access this.");
		return message.then((value) => {
			return value;
		});


};
module.exports.archiveProduct = (reqParams, reqBody) => {


		let updatedProduct = {
			isActive: reqBody.isActive
		};


		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});


};