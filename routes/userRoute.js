const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/authenticate", (req, res) => {
	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/:userId/userDetails", auth.verify, (req, res) => {

	userController.getProfile(req.params).then(resultFromController => res.send(resultFromController));
});
router.post("/checkout",auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	};

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;