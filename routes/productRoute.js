const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

// Add product for admin only
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Get all active products
router.get("/", (req, res) => {

	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// get specific product

router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId",auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});
router.put("/:productId/archive",auth.verify, (req, res) => {

	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});
module.exports = router;